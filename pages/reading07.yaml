title:      "Reading 07: Trees"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, we will work on problems related to [trees] and in particular
    [binary trees], [heaps], and [binary search trees].  Understanding how to
    represent, manipulate, and operating on these data structures will be
    necessary to solve [Challenge 13] and [Challenge 14].

    [Challenge 13]: challenge13.html
    [Challenge 14]: challenge14.html

    [trees]:                https://en.wikipedia.org/wiki/Binary_tree
    [binary trees]:         https://en.wikipedia.org/wiki/Binary_tree
    [heaps]:                https://en.wikipedia.org/wiki/Heap_(data_structure)
    [binary search trees]:  https://en.wikipedia.org/wiki/Binary_search_tree

    ## Reading

    The readings for this week are:

    1. [Competitive Programmer's Handbook]

        - <p>Chapter 14 - Tree Algorithms</p>
        - <p>Chapter 18 - Tree Queries</p>

    2. [Open Data Structures](http://opendatastructures.org/ods-cpp/)

        - <p>[6. Binary Trees](http://opendatastructures.org/ods-cpp/6_Binary_Trees.html)</p>
        - <p>[10. Heaps](http://opendatastructures.org/ods-cpp/10_Heaps.html)

    [C++]:      https://isocpp.org/
    [Python]:   https://www.python.org/

    ## Quiz

    Once you have done the readings, answer the following [Reading 07 Quiz]
    questions:

    <div id="quiz-questions"></div>

    <div id="quiz-responses"></div>

    <script src="static/js/dredd-quiz.js"></script>
    <script>
    loadQuiz('static/json/reading07.json');
    </script>

    ## Submission

    To submit you work, follow the same process outlined in [Reading 00]:

        :::bash
        $ git checkout master                 # Make sure we are in master branch
        $ git pull --rebase                   # Make sure we are up-to-date with GitLab

        $ git checkout -b reading07           # Create reading07 branch and check it out

        $ cd reading07                        # Go into reading07 folder
        $ $EDITOR answers.json                # Edit your answers.json file

        $ ../.scripts/submit.py               # Check reading07 quiz
        Submitting reading07 assignment ...
        Submitting reading07 quiz ...
              Q1 0.70
              Q2 0.40
              Q3 0.30
              Q4 0.30
              Q5 0.30
           Score 2.00

        $ git add answers.json                # Add answers.json to staging area
        $ git commit -m "Reading 07: Done"    # Commit work

        $ git push -u origin reading07        # Push branch to GitLab

    Remember to [create a merge request] and assign the appropriate TA from the
    [Reading 07 TA List].

    [GitLab]:                               https://gitlab.com
    [Reading 00]:                           reading00.html
    [Reading 07 Quiz]:                      static/json/reading07.json
    [JSON]:                                 http://www.json.org/
    [git-branch]:                           https://git-scm.com/docs/git-branch
    [dredd]:                                https://dredd.h4x0r.space
    [create a merge request]:               https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [Reading 07 TA List]:                   reading07_tas.html
    [Competitive Programmer's Handbook]:    https://cses.fi/book.html
