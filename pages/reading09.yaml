title:      "Reading 09: Graphs (MST, Topological Sort)"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, we will work on problems related to [graphs] and will focus on
    computing [minimum spanning trees] and [topological sorting] graphs.
    Understanding these concepts will be necessary to solve [Challenge 17] and
    [Challenge 18].

    [Challenge 17]: challenge17.html
    [Challenge 18]: challenge18.html
    [graphs]:       https://en.wikipedia.org/wiki/Graph_(abstract_data_type)

    [minimum spanning trees]:   https://en.wikipedia.org/wiki/Minimum_spanning_tree
    [topological sorting]:      https://en.wikipedia.org/wiki/Topological_sorting

    ## Reading

    The readings for this week are:

    1. [Competitive Programmer's Handbook]

        - <p>15 Spanning Trees</p>
        - <p>16 Directed Graphs</p>

    [C++]:      https://isocpp.org/
    [Python]:   https://www.python.org/

    ## Quiz

    Once you have done the readings, answer the following [Reading 09 Quiz]
    questions:

    <div id="quiz-questions"></div>

    <div id="quiz-responses"></div>

    <script src="static/js/dredd-quiz.js"></script>
    <script>
    loadQuiz('static/json/reading09.json');
    </script>

    ## Submission

    To submit you work, follow the same process outlined in [Reading 00]:

        :::bash
        $ git checkout master                 # Make sure we are in master branch
        $ git pull --rebase                   # Make sure we are up-to-date with GitLab

        $ git checkout -b reading09           # Create reading09 branch and check it out

        $ cd reading09                        # Go into reading09 folder
        $ $EDITOR answers.json                # Edit your answers.json file

        $ ../.scripts/submit.py               # Check reading09 quiz
        Submitting reading09 assignment ...
        Submitting reading09 quiz ...
             Q01 0.50
             Q02 0.50
             Q03 0.50
             Q04 0.50
           Score 2.00

        $ git add answers.json                # Add answers.json to staging area
        $ git commit -m "Reading 09: Done"    # Commit work

        $ git push -u origin reading09        # Push branch to GitLab

    Remember to [create a merge request] and assign the appropriate TA from the
    [Reading 09 TA List].

    [GitLab]:                               https://gitlab.com
    [Reading 00]:                           reading00.html
    [Reading 09 Quiz]:                      static/json/reading09.json
    [JSON]:                                 http://www.json.org/
    [git-branch]:                           https://git-scm.com/docs/git-branch
    [dredd]:                                https://dredd.h4x0r.space
    [create a merge request]:               https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [Reading 09 TA List]:                   reading09_tas.html
    [Competitive Programmer's Handbook]:    https://cses.fi/book.html
    [Data Structures & Algorithm Analysis]: http://people.cs.vt.edu/~shaffer/Book/C++3elatest.pdf
