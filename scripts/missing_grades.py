#!/usr/bin/env python3

import collections
import csv
import glob
import os
import yaml

# Constants

GRADES   = 'static/csv/FA18_CSE_30872_01.csv'
TAS      = 'static/yaml/ta.yaml'
MAPPINGS = 'static/yaml/reading*.yaml'

# Grades

def load_grades(path):
    grades = {}
    for student in csv.DictReader(open(path)):
        data = {}
        for k, v in student.items():
            k = ''.join(k.lower().split()).split('(')[0]
            data[k] = v
        grades[data['studentid']] = data

    return grades

# Mappings

def load_mappings(pattern):
    mappings = {}
    for path in glob.glob(pattern):
        name  = os.path.basename(path).split('.')[0]
        data  = collections.defaultdict(list)
        for student, ta in yaml.load(open(path)):
            data[ta].append(student)
        
        mappings[name] = data
        if name == 'reading00':
            mappings['challenge00'] = data
        else:
            index = int(name[-2:])
            mappings['challenge{:02d}'.format(index*2 - 1)] = data
            mappings['challenge{:02d}'.format(index*2    )] = data

    return mappings

def find_missing(mappings, grades):
    missing = collections.defaultdict(dict)

    for assignment, mapping in mappings.items():
        for ta, students in mapping.items():
            missing[ta][assignment] = []
            for student in students:
                try:
                    if not grades[student][assignment]:
                        missing[ta][assignment].append(student)
                except KeyError:
                    pass

    return missing

# Main Execution

if __name__ == '__main__':
    grades   = load_grades(GRADES)
    mappings = load_mappings(MAPPINGS)
    missing  = find_missing(mappings, grades)

    for ta, assignments in sorted(missing.items()):
        print(ta)
        for assignment, students in sorted(assignments.items()):
            if students:
                print('{:>15}: {}'.format(assignment, ', '.join(students)))
        print()

