# To Build: docker build --no-cache -t pbui/yasb . < Dockerfile

FROM	    alpine
MAINTAINER  Peter Bui <pbui@nd.edu>

RUN	    apk update

# Run-time dependencies
RUN	    apk add make python3 py3-tornado py3-requests py3-yaml py3-markdown py3-dateutil
